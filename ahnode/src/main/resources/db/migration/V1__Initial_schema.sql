CREATE TABLE source_feed (
        id uuid PRIMARY KEY,
        date_created timestamp,
        date_updated timestamp,
        baseUrl varchar(256)
);
