package org.alerthub.node.core;

import java.util.UUID;

import org.alerthub.node.utils.UUIDUtils;

public interface Constants {
	public static interface UUIDs {
		public static final UUID NAMESPACE_AHNODE = UUIDUtils.nameUUIDFromNamespaceAndString(UUIDUtils.NAMESPACE_DNS, "org.alert-hub");
	}
	public static interface Environment {
		public static final String DEMO = "demo";
	}
}
