@rem
@rem Copyright 2015 the original author or authors.
@rem
@rem Licensed under the Apache License, Version 2.0 (the "License");
@rem you may not use this file except in compliance with the License.
@rem You may obtain a copy of the License at
@rem
@rem      https://www.apache.org/licenses/LICENSE-2.0
@rem
@rem Unless required by applicable law or agreed to in writing, software
@rem distributed under the License is distributed on an "AS IS" BASIS,
@rem WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
@rem See the License for the specific language governing permissions and
@rem limitations under the License.
@rem

@if "%DEBUG%"=="" @echo off
@rem ##########################################################################
@rem
@rem  ahnode startup script for Windows
@rem
@rem ##########################################################################

@rem Set local scope for the variables with windows NT shell
if "%OS%"=="Windows_NT" setlocal

set DIRNAME=%~dp0
if "%DIRNAME%"=="" set DIRNAME=.
@rem This is normally unused
set APP_BASE_NAME=%~n0
set APP_HOME=%DIRNAME%..

@rem Resolve any "." and ".." in APP_HOME to make it shorter.
for %%i in ("%APP_HOME%") do set APP_HOME=%%~fi

@rem Add default JVM options here. You can also use JAVA_OPTS and AHNODE_OPTS to pass JVM options to this script.
set DEFAULT_JVM_OPTS=

@rem Find java.exe
if defined JAVA_HOME goto findJavaFromJavaHome

set JAVA_EXE=java.exe
%JAVA_EXE% -version >NUL 2>&1
if %ERRORLEVEL% equ 0 goto execute

echo.
echo ERROR: JAVA_HOME is not set and no 'java' command could be found in your PATH.
echo.
echo Please set the JAVA_HOME variable in your environment to match the
echo location of your Java installation.

goto fail

:findJavaFromJavaHome
set JAVA_HOME=%JAVA_HOME:"=%
set JAVA_EXE=%JAVA_HOME%/bin/java.exe

if exist "%JAVA_EXE%" goto execute

echo.
echo ERROR: JAVA_HOME is set to an invalid directory: %JAVA_HOME%
echo.
echo Please set the JAVA_HOME variable in your environment to match the
echo location of your Java installation.

goto fail

:execute
@rem Setup the command line

set CLASSPATH=%APP_HOME%\lib\ahnode.jar;%APP_HOME%\lib\micronaut-jackson-xml-4.0.1.jar;%APP_HOME%\lib\micronaut-serde-jackson-2.2.4.jar;%APP_HOME%\lib\micronaut-security-jwt-4.1.0.jar;%APP_HOME%\lib\micronaut-cache-caffeine-4.0.2.jar;%APP_HOME%\lib\micronaut-data-jdbc-4.1.2.jar;%APP_HOME%\lib\micronaut-data-processor-4.1.2.jar;%APP_HOME%\lib\micronaut-data-r2dbc-4.1.2.jar;%APP_HOME%\lib\micronaut-flyway-6.1.0.jar;%APP_HOME%\lib\micronaut-graphql-4.0.2.jar;%APP_HOME%\lib\micronaut-r2dbc-core-5.0.1.jar;%APP_HOME%\lib\micronaut-reactor-http-client-3.0.2.jar;%APP_HOME%\lib\micronaut-security-4.1.0.jar;%APP_HOME%\lib\micronaut-validation-4.0.3.jar;%APP_HOME%\lib\micronaut-reactor-3.0.2.jar;%APP_HOME%\lib\micronaut-jdbc-hikari-5.0.1.jar;%APP_HOME%\lib\micronaut-cache-core-4.0.2.jar;%APP_HOME%\lib\micronaut-data-runtime-4.1.2.jar;%APP_HOME%\lib\micronaut-data-tx-jdbc-4.1.2.jar;%APP_HOME%\lib\micronaut-data-connection-jdbc-4.1.2.jar;%APP_HOME%\lib\micronaut-jdbc-5.0.1.jar;%APP_HOME%\lib\micronaut-data-model-4.1.2.jar;%APP_HOME%\lib\micronaut-serde-support-2.2.4.jar;%APP_HOME%\lib\micronaut-serde-api-2.2.4.jar;%APP_HOME%\lib\micronaut-data-tx-4.1.2.jar;%APP_HOME%\lib\micronaut-data-connection-4.1.2.jar;%APP_HOME%\lib\micronaut-security-annotations-4.1.0.jar;%APP_HOME%\lib\micronaut-http-client-4.1.5.jar;%APP_HOME%\lib\micronaut-management-4.1.5.jar;%APP_HOME%\lib\reactor-extra-3.5.1.jar;%APP_HOME%\lib\swagger-annotations-2.2.15.jar;%APP_HOME%\lib\micronaut-websocket-4.1.5.jar;%APP_HOME%\lib\micronaut-http-client-core-4.1.5.jar;%APP_HOME%\lib\micronaut-runtime-4.1.5.jar;%APP_HOME%\lib\micronaut-discovery-core-4.1.5.jar;%APP_HOME%\lib\micronaut-jackson-databind-4.1.5.jar;%APP_HOME%\lib\micronaut-jackson-core-4.1.5.jar;%APP_HOME%\lib\micronaut-json-core-4.1.5.jar;%APP_HOME%\lib\micronaut-http-server-netty-4.1.5.jar;%APP_HOME%\lib\micronaut-http-netty-4.1.5.jar;%APP_HOME%\lib\micronaut-http-server-4.1.5.jar;%APP_HOME%\lib\micronaut-router-4.1.5.jar;%APP_HOME%\lib\micronaut-http-4.1.5.jar;%APP_HOME%\lib\micronaut-context-propagation-4.1.5.jar;%APP_HOME%\lib\micronaut-retry-4.1.5.jar;%APP_HOME%\lib\micronaut-context-4.1.5.jar;%APP_HOME%\lib\micronaut-aop-4.1.5.jar;%APP_HOME%\lib\micronaut-buffer-netty-4.1.5.jar;%APP_HOME%\lib\micronaut-inject-4.1.5.jar;%APP_HOME%\lib\jakarta.annotation-api-2.1.1.jar;%APP_HOME%\lib\logback-classic-1.4.11.jar;%APP_HOME%\lib\logback-core-1.4.11.jar;%APP_HOME%\lib\jakarta.persistence-api-3.1.0.jar;%APP_HOME%\lib\lucene-queryparser-9.7.0.jar;%APP_HOME%\lib\log4j-over-slf4j-2.0.9.jar;%APP_HOME%\lib\postgresql-42.6.0.jar;%APP_HOME%\lib\r2dbc-postgresql-1.0.1.RELEASE.jar;%APP_HOME%\lib\r2dbc-pool-1.0.0.RELEASE.jar;%APP_HOME%\lib\snakeyaml-2.0.jar;%APP_HOME%\lib\lucene-sandbox-9.7.0.jar;%APP_HOME%\lib\lucene-queries-9.7.0.jar;%APP_HOME%\lib\lucene-core-9.7.0.jar;%APP_HOME%\lib\caffeine-3.1.7.jar;%APP_HOME%\lib\jackson-dataformat-xml-2.15.2.jar;%APP_HOME%\lib\flyway-core-9.21.2.jar;%APP_HOME%\lib\jackson-dataformat-toml-2.15.2.jar;%APP_HOME%\lib\jackson-datatype-jdk8-2.15.2.jar;%APP_HOME%\lib\jackson-datatype-jsr310-2.15.2.jar;%APP_HOME%\lib\jackson-databind-2.15.2.jar;%APP_HOME%\lib\jackson-annotations-2.15.2.jar;%APP_HOME%\lib\jackson-core-2.15.2.jar;%APP_HOME%\lib\reactor-netty-core-1.1.9.jar;%APP_HOME%\lib\netty-handler-proxy-4.1.97.Final.jar;%APP_HOME%\lib\netty-codec-http2-4.1.97.Final.jar;%APP_HOME%\lib\netty-codec-http-4.1.97.Final.jar;%APP_HOME%\lib\netty-codec-socks-4.1.97.Final.jar;%APP_HOME%\lib\netty-resolver-dns-native-macos-4.1.97.Final-osx-x86_64.jar;%APP_HOME%\lib\netty-resolver-dns-classes-macos-4.1.97.Final.jar;%APP_HOME%\lib\netty-resolver-dns-4.1.97.Final.jar;%APP_HOME%\lib\netty-handler-4.1.97.Final.jar;%APP_HOME%\lib\netty-codec-dns-4.1.97.Final.jar;%APP_HOME%\lib\netty-codec-4.1.97.Final.jar;%APP_HOME%\lib\netty-transport-native-epoll-4.1.97.Final-linux-x86_64.jar;%APP_HOME%\lib\netty-transport-classes-epoll-4.1.97.Final.jar;%APP_HOME%\lib\netty-transport-native-unix-common-4.1.97.Final.jar;%APP_HOME%\lib\netty-transport-4.1.97.Final.jar;%APP_HOME%\lib\netty-buffer-4.1.97.Final.jar;%APP_HOME%\lib\reactor-pool-1.0.1.jar;%APP_HOME%\lib\reactor-core-3.5.9.jar;%APP_HOME%\lib\r2dbc-spi-1.0.0.RELEASE.jar;%APP_HOME%\lib\graphql-java-20.4.jar;%APP_HOME%\lib\micronaut-core-reactive-4.1.5.jar;%APP_HOME%\lib\reactive-streams-1.0.4.jar;%APP_HOME%\lib\micronaut-core-4.1.5.jar;%APP_HOME%\lib\jakarta.transaction-api-2.0.1.jar;%APP_HOME%\lib\HikariCP-5.0.1.jar;%APP_HOME%\lib\java-dataloader-3.2.0.jar;%APP_HOME%\lib\slf4j-api-2.0.9.jar;%APP_HOME%\lib\nimbus-jose-jwt-9.31.jar;%APP_HOME%\lib\client-2.1.jar;%APP_HOME%\lib\jakarta.validation-api-3.0.2.jar;%APP_HOME%\lib\netty-resolver-4.1.97.Final.jar;%APP_HOME%\lib\netty-common-4.1.97.Final.jar;%APP_HOME%\lib\jakarta.inject-api-2.0.1.jar;%APP_HOME%\lib\checker-qual-3.36.0.jar;%APP_HOME%\lib\error_prone_annotations-2.20.0.jar;%APP_HOME%\lib\gson-2.10.1.jar;%APP_HOME%\lib\woodstox-core-6.5.1.jar;%APP_HOME%\lib\stax2-api-4.2.1.jar;%APP_HOME%\lib\common-2.1.jar;%APP_HOME%\lib\jcip-annotations-1.0-1.jar;%APP_HOME%\lib\saslprep-1.1.jar;%APP_HOME%\lib\stringprep-1.1.jar


@rem Execute ahnode
"%JAVA_EXE%" %DEFAULT_JVM_OPTS% %JAVA_OPTS% %AHNODE_OPTS%  -classpath "%CLASSPATH%" org.olf.dcb.Application %*

:end
@rem End local scope for the variables with windows NT shell
if %ERRORLEVEL% equ 0 goto mainEnd

:fail
rem Set variable AHNODE_EXIT_CONSOLE if you need the _script_ return code instead of
rem the _cmd.exe /c_ return code!
set EXIT_CODE=%ERRORLEVEL%
if %EXIT_CODE% equ 0 set EXIT_CODE=1
if not ""=="%AHNODE_EXIT_CONSOLE%" exit %EXIT_CODE%
exit /b %EXIT_CODE%

:mainEnd
if "%OS%"=="Windows_NT" endlocal

:omega
